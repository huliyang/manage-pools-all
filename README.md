# manage-pools-all

#### 介绍
一个简单版本的线程池动态刷新与监控工具

#### 安装教程

克隆代码后，把starter模块给install到本地仓库

#### 使用说明

1. 引入maven依赖
```maven
<dependency>
    <groupId>com.fox</groupId>
    <artifactId>manage-pools-starter</artifactId>
    <version>${revision}</version>
</dependency>
```
2. nacos配置文件
```yaml
spring:
  dtp:
    enabled: true               #是否开启线程池
    ding-ding:
      accessToken:              #钉钉的token
      atMobiles:                #需要@的手机号码，使用逗号分隔
      atAll: false              #是否@所有人
    executors:                                   #动态线程池配置
      - thread-pool-name: 'message-consume'      #bean的名称
        # 核心线程数
        core-pool-size: 3
        # 最大线程数
        maximum-pool-size: 10
        # 阻塞队列名称，参考 BlockingQueueTypeEnum
        blocking-queue: 'LinkedBlockingQueue'
        # 阻塞队列大小
        queue-capacity: 1
        # 拒绝策略名称，参考 RejectedPolicyTypeEnum
        rejected-handler: 'AbortPolicy'
        # 线程存活时间，单位秒
        keep-alive-time: 1024
        # 线程工厂名称前缀
        thread-name-prefix: 'message-consume'
        # 是否报警
        alarm: true
        # 活跃度报警阈值；假设线程池最大线程数 10，当线程数达到 8 发起报警
        active-alarm: 80
        # 报警间隔，同一线程池下同一报警纬度，在 interval 时间内只会报警一次，单位分钟，不支持0.x写法
        interval: 8
```
3. 启动类加@EnableDtp注解


4. 使用@Resource 或@Autowired 进行依赖注入，bean的名称是配置文件里面的name。或通过 DtpRegistry.getDtpExecutor("name") 获取

5. 告警功能图片

使用DtpRegistry的sendDingDing()方法钉钉发送消息图示

<img src="https://img1.imgtp.com/2023/07/12/1COGZcWp.png" alt="线程池参数获取.png" title="线程池参数获取.png" />

线程池参数告警钉钉发送消息图示

<img src="https://img1.imgtp.com/2023/07/12/ndg2ChYK.png" alt="线程池告警.png" title="线程池告警.png" />

nacos修改后钉钉发送消息图示

<img src="https://img1.imgtp.com/2023/07/12/FprKUaCf.png" alt="nacos修改.png" title="nacos修改.png" />

#### 项目说明
项目参考了美团线程池文章

https://tech.meituan.com/2020/04/02/java-pooling-pratice-in-meituan.html

项目参考了dynamic-tp线程池实现

https://dynamictp.cn/