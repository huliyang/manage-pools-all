package com.fox.managepoolscommon.util;


import cn.hutool.json.ObjectMapper;
import com.fox.managepoolscommon.consts.DtpConst;
import com.fox.managepoolscommon.em.ConfigFileTypeEnum;
import com.fox.managepoolscommon.yml.DtpProperties;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.ByteArrayResource;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

public class NacosContentUtil {

    public static Map<Object,Object> contentToMap(String content, ConfigFileTypeEnum configFileType){
        switch (configFileType) {
            case YML:return doParseYML(content);
            case PROPERTIES: return doParsePROPERTIES(content);
            case YAML:return doParseYML(content);
            default:return null;
        }
    }

    public static void bindDtpProperties(Map<?, Object> properties, DtpProperties dtpProperties) {
        ConfigurationPropertySource sources = new MapConfigurationPropertySource(properties);
        Binder binder = new Binder(sources);
        ResolvableType type = ResolvableType.forClass(DtpProperties.class);
        Bindable<?> target = Bindable.of(type).withExistingValue(dtpProperties);
        binder.bind(DtpConst.MAIN_PROPERTIES_PREFIX, target);
    }

    public static Map<Object, Object> doParseYML(String content) {

        if (StringUtils.isEmpty(content)) {
            return Collections.emptyMap();
        }
        YamlPropertiesFactoryBean bean = new YamlPropertiesFactoryBean();
        bean.setResources(new ByteArrayResource(content.getBytes()));
        return bean.getObject();
    }

    public static Map<Object, Object> doParsePROPERTIES(String content){

        if (StringUtils.isBlank(content)) {
            return Collections.emptyMap();
        }
        Properties properties = new Properties();
        try {
            properties.load(new StringReader(content));
        } catch (IOException e) {
            return null;
        }
        return properties;
    }

}
