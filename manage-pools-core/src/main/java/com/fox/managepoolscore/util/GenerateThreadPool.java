package com.fox.managepoolscore.util;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import com.fox.managepoolscommon.consts.DtpConst;
import com.fox.managepoolscommon.em.QueueTypeEnum;
import com.fox.managepoolscommon.em.RejectedTypeEnum;
import com.fox.managepoolscommon.entity.DtpExecutorProps;
import com.fox.managepoolscore.dtppackage.ThreadFactoryImpl;

import java.util.concurrent.*;

public class GenerateThreadPool {
    public static ThreadPoolExecutor dtpExecutorPropsToTp(DtpExecutorProps dtpExecutorProps) {
        if (dtpExecutorProps.coreParamIsInValid()) {
            return defaultThreadPoolExecutor();
        }
        ThreadPoolExecutor threadPoolExecutor =
                new ThreadPoolExecutor(
                        dtpExecutorProps.getCorePoolSize(),
                        dtpExecutorProps.getMaximumPoolSize(),
                        dtpExecutorProps.getKeepAliveTime(),
                        dtpExecutorProps.getUnit(),
                        QueueTypeEnum.buildLbq(dtpExecutorProps.getBlockingQueue().getName(),dtpExecutorProps.getQueueCapacity()),
                        new ThreadFactoryImpl(dtpExecutorProps.getThreadNamePrefix()),
                        RejectedTypeEnum.buildHandle(dtpExecutorProps.getRejectedHandler())
                );
        return threadPoolExecutor;
    }

    //为一些参数无法通过的提供默认模板
    private static ThreadPoolExecutor defaultThreadPoolExecutor(){
        int i = DtpConst.AVAILABLE_PROCESSORS;
        //接近jdk提供的Executors.newFixedThreadPool()方法
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(i, i, 0L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        return threadPoolExecutor;
    }

}
