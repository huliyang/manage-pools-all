package com.fox.managepoolscore.dtppackage;

import lombok.EqualsAndHashCode;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

@EqualsAndHashCode
public class ThreadFactoryImpl implements ThreadFactory {

    private final AtomicInteger seq = new AtomicInteger(0);

    private String threadName;

    public ThreadFactoryImpl(String threadName){
        this.threadName = threadName;
    }

    @Override
    public Thread newThread(Runnable r) {
        String name = threadName + "_" + seq.getAndIncrement();
        return new Thread(name);
    }
}
