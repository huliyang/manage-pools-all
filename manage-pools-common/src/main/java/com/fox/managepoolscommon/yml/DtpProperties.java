package com.fox.managepoolscommon.yml;

import com.fox.managepoolscommon.consts.DtpConst;
import com.fox.managepoolscommon.entity.DingdingEntity;
import com.fox.managepoolscommon.entity.DtpExecutorProps;
import lombok.Data;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = DtpConst.MAIN_PROPERTIES_PREFIX)
public class DtpProperties {

    public DtpProperties() {
        Properties instance = Properties.getInstance();
        instance.setDtpProperties(this);
    }

    private boolean enabled;

    /**
     * DingdingEntity platform configs.
     */
    private DingdingEntity dingDing;

    /**
     * ThreadPoolExecutor configs.
     */
    private List<DtpExecutorProps> executors;


}
