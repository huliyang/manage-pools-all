package com.fox.managepoolscommon.yml;

import lombok.Data;

@Data
public class Properties {

    private static Properties properties= new Properties();

    private DtpProperties dtpProperties;

    private OtherProperties otherProperties;

    private Properties() {
    }
    public static Properties getInstance(){
        return properties;
    }
}
