package com.fox.managepoolscore;

import com.fox.managepoolscommon.entity.DtpExecutorProps;
import com.fox.managepoolscommon.yml.DtpProperties;
import com.fox.managepoolscommon.yml.Properties;
import com.fox.managepoolscore.dtppackage.ExecutorWrapper;
import com.fox.managepoolscore.handler.DingDingHandler;
import com.fox.managepoolscore.time.ThresholdAlarmTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;

public class DtpRegistry implements ApplicationRunner, ApplicationContextAware {

    //线程池ID，线程池实例
    private static final Map<String, ExecutorWrapper> EXECUTOR_REGISTRY = new ConcurrentHashMap<>();
    //线程池ID，通知实例
    private static final Map<String, ThresholdAlarmTimer> TIMER_REGISTRY = new ConcurrentHashMap<>();

    private DtpProperties dtpProperties;

    private ConfigurableBeanFactory beanFactory;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.beanFactory = (ConfigurableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
    }

    public DtpRegistry() {
    }

    //把线程池注入spring的bean容器当中
    public void registerBean(String beanName, Object objectInstance) {
        if (beanFactory==null)return;
        beanFactory.registerSingleton(beanName, objectInstance);
    }

    //获取线程池
    public static ThreadPoolExecutor getThreadPoolExecutor(String ThreadPoolName){
        ExecutorWrapper executorWrapper = EXECUTOR_REGISTRY.get(ThreadPoolName);
 	return executorWrapper==null?null:executorWrapper.getThreadPoolExecutor();
    }

    //根据名字获取信息发送钉钉
    public void idGetParameter(String ThreadPoolName){
        DingDingHandler dingDingHandler = new DingDingHandler();
        ExecutorWrapper executorWrapper = EXECUTOR_REGISTRY.get(ThreadPoolName);
        if (executorWrapper==null) {
            return;
        }
        String massage = dingDingHandler.buildThreadPoolParameters(executorWrapper);
        dingDingHandler.send(massage);
    }

    //定时任务发送的，发送前会进行参数校验
    public void thresholdAlarm(String ThreadPoolName){
        DingDingHandler dingDingHandler = new DingDingHandler();
        ExecutorWrapper executorWrapper = EXECUTOR_REGISTRY.get(ThreadPoolName);
        if (executorWrapper==null) {
            return;
        }
        String massage = dingDingHandler.buildThreadPoolAlarm(executorWrapper);
        dingDingHandler.send(massage);
    }

    public void modificationNotification(Set<String> threadPoolNames){
        if (threadPoolNames==null||threadPoolNames.isEmpty()) {
            return;
        }
        DingDingHandler dingDingHandler = new DingDingHandler();
        String massage = dingDingHandler.buildNoticeContent(threadPoolNames);
        dingDingHandler.send(massage);
    }
    @Override
    public void run(ApplicationArguments args){
        Properties instance = Properties.getInstance();
        this.dtpProperties = instance.getDtpProperties();
        if (dtpProperties==null||dtpProperties.getExecutors()==null||dtpProperties.getDingDing()==null||dtpProperties.getExecutors().isEmpty())return;
        for (DtpExecutorProps executor : dtpProperties.getExecutors()) {
            ExecutorWrapper executorWrapper = new ExecutorWrapper(executor);
            TIMER_REGISTRY.put(executorWrapper.getThreadPoolName(),new ThresholdAlarmTimer(executorWrapper,this));
            EXECUTOR_REGISTRY.put(executorWrapper.getThreadPoolName(),executorWrapper);
            registerBean(executorWrapper.getThreadPoolName(),executorWrapper.getThreadPoolExecutor());
        }
    }

    //刷新线程池参数
    public void refreshThreadPool() throws Exception{
        Set<String> threadPoolNames = new HashSet<>();
        DtpProperties dtpProperties = Properties.getInstance().getDtpProperties();
        if (Objects.isNull(dtpProperties)||Objects.isNull(dtpProperties.getExecutors())) {
            return;
        }

        //循环刷新线程池参数
        for (DtpExecutorProps executor : dtpProperties.getExecutors()) {
            String threadPoolName = executor.getThreadPoolName();
            ExecutorWrapper executorWrapper = EXECUTOR_REGISTRY.get(threadPoolName);
            //不为空且参数正确
            if (executorWrapper==null||executor.coreParamIsInValid()) {
                continue;
            }

            //如果任何消息都不更新
            if (executorWrapper.threadPoolExecutorEquels(executor)) {
                continue;
            }

            //需要刷新的线程池
            threadPoolNames.add(threadPoolName);
            executorWrapper.setDtpExecutorProps(executor);
            executorWrapper.refreshThreadPool();
        }
        //发送修改消息
        if (!threadPoolNames.isEmpty()) {
            modificationNotification(threadPoolNames);
        }
    }


}
