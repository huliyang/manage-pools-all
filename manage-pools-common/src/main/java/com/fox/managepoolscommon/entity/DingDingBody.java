package com.fox.managepoolscommon.entity;


import lombok.Data;

import java.util.List;

//写一个简单版本的text的请求体
@Data
public class DingDingBody {


    private String msgtype = "text";

    private Text text;

    private At at;


    @Data
    public static class Text {
        //消息内容
        private String content;
    }

    /**
     * At info.
     */
    public static class At {
        //请求手机号
        private List<String> atMobiles;
        //是否@所有人
        private boolean isAtAll = false;

        public List<String> getAtMobiles() {
            return atMobiles;
        }

        public void setAtMobiles(List<String> atMobiles) {
            this.atMobiles = atMobiles;
        }

        public boolean getIsAtAll() {
            return isAtAll;
        }

        public void setIsAtAll(boolean atAll) {
            isAtAll = atAll;
        }
    }

}
