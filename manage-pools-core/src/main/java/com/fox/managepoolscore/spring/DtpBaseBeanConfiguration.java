package com.fox.managepoolscore.spring;

import com.fox.managepoolscommon.yml.DtpProperties;
import com.fox.managepoolscore.DtpRegistry;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;

//可以理解为懒加载下面的bean
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties({DtpProperties.class})
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class DtpBaseBeanConfiguration {

    @Bean
    public DtpRegistry dtpRegistry() {
        return new DtpRegistry();
    }
}