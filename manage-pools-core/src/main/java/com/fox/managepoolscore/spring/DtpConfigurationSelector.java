/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fox.managepoolscore.spring;

import com.fox.managepoolscommon.yml.OtherProperties;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import static com.fox.managepoolscommon.consts.DtpConst.DTP_ENABLED_PROP;


public class DtpConfigurationSelector implements DeferredImportSelector, Ordered, EnvironmentAware {

    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    //扫描生成bean
    @Override
    public String[] selectImports(AnnotationMetadata metadata) {
        //如果enable是关闭
        if (!BooleanUtils.toBoolean(environment.getProperty(DTP_ENABLED_PROP, BooleanUtils.TRUE))) {
            return new String[]{};
        }
        //获取以下生成bean
        return new String[]{
                DtpBaseBeanConfiguration.class.getName(),
                OtherProperties.class.getName()
        };
    }

    //最先载入
    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }

}
