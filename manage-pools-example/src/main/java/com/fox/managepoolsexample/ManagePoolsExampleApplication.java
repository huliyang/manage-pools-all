package com.fox.managepoolsexample;

import com.fox.managepoolscommon.yml.DtpProperties;
import com.fox.managepoolscommon.yml.Properties;
import com.fox.managepoolscore.DtpRegistry;
import com.fox.managepoolscore.spring.EnableDtp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableDtp
public class ManagePoolsExampleApplication {


    public static void main(String[] args) {
        SpringApplication.run(ManagePoolsExampleApplication.class, args);
    }

}
