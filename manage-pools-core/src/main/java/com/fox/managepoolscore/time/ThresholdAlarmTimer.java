package com.fox.managepoolscore.time;

import com.fox.managepoolscommon.util.CountUtil;
import com.fox.managepoolscore.DtpRegistry;
import com.fox.managepoolscore.dtppackage.ExecutorWrapper;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadPoolExecutor;

public class ThresholdAlarmTimer extends Timer {

    private ExecutorWrapper executorWrapper;
    private DtpRegistry dtpRegistry;

    public ThresholdAlarmTimer(ExecutorWrapper executorWrapper, DtpRegistry dtpRegistry) {
        this.executorWrapper = executorWrapper;
        this.dtpRegistry = dtpRegistry;
        //定时任务实现,底层就是一个一直为true的循环多线程实现,*60000为分钟
        int delayAndPeriod = executorWrapper.getInterval() * 60000;
        schedule(new TimerTaskImpl(),delayAndPeriod,delayAndPeriod);
    }

    public void setExecutorWrapper(ExecutorWrapper executorWrapper) {
        this.executorWrapper = executorWrapper;
    }


    public class TimerTaskImpl extends TimerTask {
        int rejectCount = 0;
        @Override
        public void run() {
            ThreadPoolExecutor threadPoolExecutor = executorWrapper.getThreadPoolExecutor();
            //判断是否开启了消息通知，如果开启了且超过了阈值或者拒绝策略上升都发送一次消息
            if (executorWrapper.getAlarm().booleanValue()
                    && (CountUtil.getPercentage(threadPoolExecutor.getActiveCount(),threadPoolExecutor.getMaximumPoolSize())>=executorWrapper.getActiveAlarm()
                    ||executorWrapper.getRejectedTaskCount()>rejectCount)) {
                //发送消息到钉钉中
                dtpRegistry.thresholdAlarm(executorWrapper.getThreadPoolName());
            }
        }
    }
}
