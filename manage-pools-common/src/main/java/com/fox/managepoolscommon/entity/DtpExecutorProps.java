package com.fox.managepoolscommon.entity;


import cn.hutool.core.lang.UUID;
import com.fox.managepoolscommon.consts.DtpConst;
import com.fox.managepoolscommon.em.QueueTypeEnum;
import com.fox.managepoolscommon.em.RejectedTypeEnum;
import lombok.Data;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Data
public class DtpExecutorProps {
    //下面是线程池相关参数
    private String threadPoolName = UUID.fastUUID().toString();

    private int corePoolSize = 1;

    private int maximumPoolSize = DtpConst.AVAILABLE_PROCESSORS;

    private int keepAliveTime = 60;

    private TimeUnit unit = TimeUnit.SECONDS;

    private QueueTypeEnum blockingQueue = QueueTypeEnum.LINKED_BLOCKING_QUEUE;

    private int queueCapacity = Integer.MAX_VALUE;

    private RejectedTypeEnum rejectedHandler = RejectedTypeEnum.DISCARD_POLICY;

    private String threadNamePrefix = "TFName";


    //下面所有都是通知相关的参数
    private Boolean alarm = true;

    private int activeAlarm = 80;


    //隔多久分钟提醒一次
    private Integer interval = 5;

    //检验参数是否合适，不适合另作打算
    public boolean coreParamIsInValid() {
        return this.getCorePoolSize() < 0
                || this.getMaximumPoolSize() <= 0
                || this.getMaximumPoolSize() < this.getCorePoolSize()
                || this.getKeepAliveTime() < 0;
    }
}
