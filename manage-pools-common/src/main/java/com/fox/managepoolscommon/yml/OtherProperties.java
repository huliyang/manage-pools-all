package com.fox.managepoolscommon.yml;

import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;

import java.net.InetAddress;
import java.net.UnknownHostException;

//其他的，项目中所需的参数
@Data
public class OtherProperties {
    public OtherProperties() {
        Properties instance = Properties.getInstance();
        instance.setOtherProperties(this);
    }

    //sever配置
    private String ip = "127.0.0.1";
    {
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
        }
    }
    @Value("${server.port:8080}")
    private int port;
    @Value("${spring.application.name:dtp}")
    private String serviceName;

    //nacos配置
    @Value("${nacos.config.type:yml}")
    private String type;

    @Value("${nacos.config.server-addr:127.0.0.1:8848}")
    private String serverAddr;
    @Value("${nacos.config.group:DEFAULT_GROUP}")
    private String group;
    @Value("${nacos.config.data-ids:dynamic.yml}")
    private String dataId;
    public String applicationeName(){
        return ip+":"+port+"/"+serviceName;
    }
    public String configureLink(){ return  "\nnacos\nserverAddr:"+serverAddr+"\ngroup:"+group+"\ndataId:"+dataId;}
}
