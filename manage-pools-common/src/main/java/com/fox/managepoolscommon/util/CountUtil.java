package com.fox.managepoolscommon.util;

import java.math.BigDecimal;

public class CountUtil {
    /**
     * 用于求百分比方法，已考虑除数不为0的情况
     * @param number1 除数
     * @param number2 被除数
     * @return 百分比%
     */
    public static int getPercentage(double number1,double number2) {
        int formatNumber = 2;
        double number100;
        if(number1!=0 && number2!=0){
            double numberA= number1 /number2 *100;//到报率求%
            number100= new BigDecimal(numberA).setScale(formatNumber, BigDecimal.ROUND_HALF_UP).doubleValue();//四舍五入保留2位小数
            return (int) number100;
        }else{
            number100=0.0;
            return (int) number100;
        }
    }
}
