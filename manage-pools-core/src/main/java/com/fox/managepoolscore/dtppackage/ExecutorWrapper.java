package com.fox.managepoolscore.dtppackage;

import com.fox.managepoolscommon.em.QueueTypeEnum;
import com.fox.managepoolscommon.em.RejectedTypeEnum;
import com.fox.managepoolscommon.entity.DtpExecutorProps;
import com.fox.managepoolscore.util.GenerateThreadPool;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Objects;
import java.util.ServiceLoader;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.LongAdder;

/**
 * 该类是对于线程池外层在进行一个包装,对于一些附加进行的封装
 * 因为线程池中缺少拒绝次数等参数
 */

@Data
public class ExecutorWrapper {

    private String threadPoolName;

    private ThreadPoolExecutor threadPoolExecutor;

    //下面所有都是通知相关的参数
    private Boolean alarm;

    private int activeAlarm;

    private Integer interval;

    //记录了拒绝次数
    private final LongAdder rejectCount = new LongAdder();

    //记录下原始参数
    private DtpExecutorProps dtpExecutorProps;

    //这里也可以进行参数校验，不在范围内的话可以进行再赋值
    public ExecutorWrapper(DtpExecutorProps dtpExecutorProps){
        this.threadPoolName = dtpExecutorProps.getThreadPoolName();
        this.threadPoolExecutor = GenerateThreadPool.dtpExecutorPropsToTp(dtpExecutorProps);
        RejectedExecutionHandlerImpl rejectedExecutionHandler = new RejectedExecutionHandlerImpl(threadPoolExecutor.getRejectedExecutionHandler());
        //让他拒绝策略之前增加拒绝次数
        threadPoolExecutor.setRejectedExecutionHandler(rejectedExecutionHandler);

        this.alarm = dtpExecutorProps.getAlarm();
        //范围不在1-100中赋值为80
        this.activeAlarm = (0<dtpExecutorProps.getActiveAlarm()&&dtpExecutorProps.getActiveAlarm()<=100)?dtpExecutorProps.getActiveAlarm():80;
        this.interval = dtpExecutorProps.getInterval();
        this.dtpExecutorProps = dtpExecutorProps;
    }

    public long getQueueSize(){
        //队列具备的容量
        int capacity = threadPoolExecutor.getQueue().size()+threadPoolExecutor.getQueue().remainingCapacity();
        capacity = capacity<0? Integer.MAX_VALUE :capacity ;
        return capacity;
    }

    public long getRejectedTaskCount() {
        return rejectCount.sum();
    }

    public void incRejectCount(int count) {
        rejectCount.add(count);
    }

    private class RejectedExecutionHandlerImpl implements RejectedExecutionHandler{
        private RejectedExecutionHandler rejectedExecutionHandler;
        public RejectedExecutionHandlerImpl(RejectedExecutionHandler rejectedExecutionHandler) {
            this.rejectedExecutionHandler = rejectedExecutionHandler;
        }

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            incRejectCount(1);
            rejectedExecutionHandler.rejectedExecution(r,executor);
        }
    }
    public void refreshThreadPool(){
        threadPoolExecutor.setThreadFactory(new ThreadFactoryImpl(dtpExecutorProps.getThreadNamePrefix()));
        threadPoolExecutor.setMaximumPoolSize(dtpExecutorProps.getMaximumPoolSize());
        threadPoolExecutor.setCorePoolSize(dtpExecutorProps.getCorePoolSize());
        threadPoolExecutor.setKeepAliveTime(dtpExecutorProps.getKeepAliveTime(),dtpExecutorProps.getUnit());
        threadPoolExecutor.setRejectedExecutionHandler(new RejectedExecutionHandlerImpl(RejectedTypeEnum.buildHandle(dtpExecutorProps.getRejectedHandler())));
    }

    public boolean threadPoolExecutorEquels(DtpExecutorProps dtpExecutorProps1){
        return dtpExecutorProps1.equals(dtpExecutorProps);
    }
}
