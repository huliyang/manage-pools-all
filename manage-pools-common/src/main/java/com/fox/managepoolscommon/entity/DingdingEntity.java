package com.fox.managepoolscommon.entity;

import lombok.Data;

import java.util.List;

//请求token+号码
@Data
public class DingdingEntity {

    private String accessToken;

    private List<String> atMobiles;

    private boolean atAll = false;

}