package com.fox.managepoolsstarter.refresher;

import cn.hutool.core.util.StrUtil;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.fox.managepoolscommon.em.ConfigFileTypeEnum;
import com.fox.managepoolscommon.util.NacosContentUtil;
import com.fox.managepoolscommon.yml.DtpProperties;
import com.fox.managepoolscommon.yml.OtherProperties;
import com.fox.managepoolscommon.yml.Properties;
import com.fox.managepoolscore.DtpRegistry;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
public class NacosRefresher  implements InitializingBean, DisposableBean, Listener {

    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(1);;

    private ConfigFileTypeEnum configFileType;

    @NacosInjected
    private ConfigService configService;

    @Resource
    private DtpRegistry dtpRegistry;

    @Override
    public void afterPropertiesSet() {
        Properties instance = Properties.getInstance();
        OtherProperties otherProperties = instance.getOtherProperties();
        //默认使用YML文件类型，为空就是yml文件类型，否则是
        configFileType = StrUtil.isEmpty(otherProperties.getType())?ConfigFileTypeEnum.YML:ConfigFileTypeEnum.of(otherProperties.getType());;
        String dataId = otherProperties.getDataId();
        String group = otherProperties.getGroup();
        try {
            configService.addListener(dataId, group, this);
            log.info("DynamicTp refresher, add listener success, dataId: {}, group: {}", dataId, group);
        } catch (NacosException e) {
            log.error("DynamicTp refresher, add listener error, dataId: {}, group: {}", dataId, group, e);
        }
    }

    @Override
    public Executor getExecutor() {
        return EXECUTOR;
    }

    //该方法是刷新的核心方法，nacos监控修改yml文件会往该方法发一个字符串
    @Override
    public void receiveConfigInfo(String content) {
        if (StringUtils.isBlank(content) || Objects.isNull(configFileType)) {
            log.warn("DynamicTp refresh, empty content or null fileType.");
            return;
        }
        Properties instance = Properties.getInstance();
        //刷新参数到Properties的DtpProperties中，然后在执行刷新函数就OK了
        DtpProperties refresh = refresh(content, configFileType);
        if (Objects.isNull(refresh)) {
            return;
        }
        instance.setDtpProperties(refresh);
        try {
            dtpRegistry.refreshThreadPool();
        } catch (Exception e) {
            log.error("DynamicTp refresher error", e);
        }
    }

    @Override
    public void destroy() {
        EXECUTOR.shutdown();
    }

    //将content根据类型转换成Dtp类型，然后刷新
    private DtpProperties refresh(String content,ConfigFileTypeEnum configFileType){
        if (StringUtils.isBlank(content) || Objects.isNull(configFileType)) {
            return null;
        }
        try {
            Map<Object, Object> objectObjectMap = NacosContentUtil.contentToMap(content, configFileType);
            if (objectObjectMap==null) {
                return null;
            }
            DtpProperties dtpProperties = new DtpProperties();
            NacosContentUtil.bindDtpProperties(objectObjectMap,dtpProperties);
            return dtpProperties;
        } catch (Exception e) {
            return null;
        }
    }

}
