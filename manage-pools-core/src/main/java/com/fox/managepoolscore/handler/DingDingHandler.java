package com.fox.managepoolscore.handler;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import com.fox.managepoolscommon.consts.DingDingConst;
import com.fox.managepoolscommon.entity.DingDingBody;
import com.fox.managepoolscommon.entity.DingdingEntity;
import com.fox.managepoolscommon.util.CountUtil;
import com.fox.managepoolscommon.yml.DtpProperties;
import com.fox.managepoolscommon.yml.OtherProperties;
import com.fox.managepoolscommon.yml.Properties;
import com.fox.managepoolscore.dtppackage.ExecutorWrapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ThreadPoolExecutor;

//钉钉发送消息
@Slf4j
@Data
public class DingDingHandler {

    private DtpProperties dtpProperties;
    private OtherProperties otherProperties;

    public DingDingHandler() {
        Properties instance = Properties.getInstance();
        this.dtpProperties = instance.getDtpProperties();
        this.otherProperties = instance.getOtherProperties();
    }

    //配置类，发送的消息
    public void send(String message){
        DingDingBody dingDingBody = new DingDingBody();
        DingDingBody.At at = new DingDingBody.At();
        DingdingEntity dingDing = dtpProperties.getDingDing();
        at.setAtMobiles(dingDing.getAtMobiles());
        at.setIsAtAll(dingDing.isAtAll());
        dingDingBody.setAt(at);
        DingDingBody.Text text = new DingDingBody.Text();
        text.setContent(message);
        dingDingBody.setText(text);
        String s = JSONUtil.toJsonStr(dingDingBody);
        try {
            HttpResponse response =  HttpRequest.post(getURL(dingDing.getAccessToken())).body(s).execute();
            if (Objects.nonNull(response)) {
                log.info("dingDing send success, response: {}, request: {}",
                        response.body(), JSONUtil.toJsonStr(dingDingBody));
            }
        } catch (Exception e) {
            log.error("dingDing send fail", e);
        }
    }

    private static String getURL(String token){
        return DingDingConst.DING_URL + token;
    }

    //根据ID获取线程池参数
    public String buildThreadPoolParameters(ExecutorWrapper executorWrapper){
        ThreadPoolExecutor threadPoolExecutor = executorWrapper.getThreadPoolExecutor();
        String format = String.format(
                DingDingConst.DING_PARAMETER_TEMPLATE,
                otherProperties.applicationeName(),
                executorWrapper.getThreadPoolName(),
                threadPoolExecutor.getCorePoolSize(),
                threadPoolExecutor.getMaximumPoolSize(),
                threadPoolExecutor.getPoolSize(),
                threadPoolExecutor.getActiveCount(),
                threadPoolExecutor.getQueue().getClass().getSimpleName(),
                threadPoolExecutor.getQueue().size(),
                threadPoolExecutor.getQueue().remainingCapacity(),
                threadPoolExecutor.getCompletedTaskCount(),
                threadPoolExecutor.getLargestPoolSize(),
                executorWrapper.getRejectedTaskCount(),
                otherProperties.configureLink()
        );
        return format;
    }

    //阀值告警参数
    public String buildThreadPoolAlarm(ExecutorWrapper executorWrapper){
        ThreadPoolExecutor threadPoolExecutor = executorWrapper.getThreadPoolExecutor();
        String format = String.format(
                DingDingConst.DING_ALARM_TEMPLATE,
                otherProperties.applicationeName(),

                CountUtil.getPercentage(threadPoolExecutor.getActiveCount(),threadPoolExecutor.getMaximumPoolSize()),executorWrapper.getActiveAlarm(),executorWrapper.getRejectedTaskCount(),

                executorWrapper.getThreadPoolName(),
                threadPoolExecutor.getCorePoolSize(),
                threadPoolExecutor.getMaximumPoolSize(),
                threadPoolExecutor.getPoolSize(),
                threadPoolExecutor.getActiveCount(),
                threadPoolExecutor.getQueue().getClass().getSimpleName(),
                threadPoolExecutor.getQueue().size(),
                threadPoolExecutor.getQueue().remainingCapacity(),
                threadPoolExecutor.getCompletedTaskCount(),
                threadPoolExecutor.getLargestPoolSize(),
                executorWrapper.getRejectedTaskCount(),
                otherProperties.configureLink(),
                executorWrapper.getInterval()
        );
        return format;
    }

    //修改发送钉钉
    public String buildNoticeContent(Set<String> threadPoolNames){
        StringBuilder stringBuilder = new StringBuilder(DingDingConst.DING_CHANGE_NOTICE_TEMPLATE);
        String format = String.format(
                stringBuilder.toString(),
                otherProperties.applicationeName(),
                otherProperties.configureLink()
        );
        stringBuilder = new StringBuilder(format);
        for (String threadPoolName : threadPoolNames) {
            stringBuilder.append(threadPoolName).append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length()-1).append("。");
        return stringBuilder.toString();
    }
}

