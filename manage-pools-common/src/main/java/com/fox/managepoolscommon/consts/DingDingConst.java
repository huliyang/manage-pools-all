package com.fox.managepoolscommon.consts;

public class DingDingConst {
    public static final String DING_URL = "https://oapi.dingtalk.com/robot/send?access_token=";
    //三种，刷新的时候发送的消息，告警的时候发送的消息，正常获取的时候发送的消息
    public static final String DING_CHANGE_NOTICE_TEMPLATE =
                    "[线上]\n" +
                    "[应用]%s\n" +
                    "[线程池参数变更]\n" +
                    "线程池配置链接:%s\n"+
                            "\n"+
                    "可以通过DtpRegistry的sendDingDing()方法获取具体参数\n"+
                    "修改的线程池名字是:";
    public static final String DING_PARAMETER_TEMPLATE =
                    "[线上]\n" +
                    "[应用]%s\n" +
                    "[线程池参数获取]\n" +
                    "poolName:%s\n" +
                    "corePoolSize:%s\n" +
                    "maximumPoolSize:%s\n" +
                    "poolSize:%s\n" +
                    "activeCount:%s\n" +
                    "queueType:%s\n" +
                    "queueSize:%s\n" +
                    "queueRemainingCapacity:%s\n" +
                    "completedTaskCount:%s\n" +
                    "largestPoolSize:%s\n" +
                    "rejectCount:%s\n" +
                    "线程池配置链接:%s";
    //触发阈值就发一次信息
    public static final String DING_ALARM_TEMPLATE =
                    "[线上]\n" +
                    "[应用]%s\n" +
                    "[线程池告警]\n" +
                    "[告警原因]\n" +
                    "activeCount/maximumPoolSize值为:%s，触发阈值(%s)，线程池中出现RejectedExecutionException(%s次数)\n" +
                    "[线程池参数获取]\n" +
                    "poolName:%s\n" +
                    "corePoolSize:%s\n" +
                    "maximumPoolSize:%s\n" +
                    "poolSize:%s\n" +
                    "activeCount:%s\n" +
                    "queueType:%s\n" +
                    "queueSize:%s\n" +
                    "queueRemainingCapacity:%s\n" +
                    "completedTaskCount:%s\n" +
                    "largestPoolSize:%s\n" +
                    "rejectCount:%s\n" +
                    "线程池配置链接:%s\n" +
                    "[告警间隔时间]%s分钟";
}
