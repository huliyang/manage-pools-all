/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fox.managepoolscommon.em;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * RejectedTypeEnum related
 *
 * @author yanhom
 * @since 1.0.0
 **/
@Slf4j
@Getter
public enum RejectedTypeEnum {

    //该策略会直接抛出异常，阻止系统正常工作。
    ABORT_POLICY("AbortPolicy"),

    //只要线程池未关闭，该策略直接在调用者线程中，运行当前被丢弃的任务。显然这样做不会真的丢弃任务，但是，任务提交线程的性能极有可能会急剧下降。
    CALLER_RUNS_POLICY("CallerRunsPolicy"),

    //该策略将丢弃最老的一个请求，也就是即将被执行的一个任务，并尝试再次提交当前任务。
    DISCARD_OLDEST_POLICY("DiscardOldestPolicy"),

    //该策略默默地丢弃无法处理的任务，不予任何处理。如果允许任务丢失，推荐使用这个。这也是默认的策略
    DISCARD_POLICY("DiscardPolicy");

    private final String name;

    RejectedTypeEnum(String name) {
        this.name = name;
    }

    //可以对于该方法进行增强操作，执行操作后+1拒绝数
    public static RejectedExecutionHandler buildHandle(RejectedTypeEnum rejectedTypeEnum){
        switch (rejectedTypeEnum){
            case ABORT_POLICY:return new ThreadPoolExecutor.AbortPolicy();
            case CALLER_RUNS_POLICY:return new ThreadPoolExecutor.CallerRunsPolicy();
            case DISCARD_OLDEST_POLICY:return new ThreadPoolExecutor.DiscardOldestPolicy();
            case DISCARD_POLICY:return new ThreadPoolExecutor.DiscardPolicy();
            default:return new ThreadPoolExecutor.DiscardPolicy();
        }
    }

}
